#import "template-bakalarka.typ": *
#show: slides.with("SeMeStRaLkA","Jakub Ko", "2024")

#set table(align: center+horizon, inset: 7pt)
#let experiment(num,body) = {
	box(width: 100%, height: auto, fill: rgb(245,245,245), radius: 15pt, inset: 15pt)[
		#set text(size: 19pt)
		*Experiment #num*:#math. #body
	]
} 


#slide(title: "Příklad A: Optimalizace uživatele zadané funkce")[
	- Zastavovací podmínka - velikost aproximace gradientu
	- Kontrola spádovosti směrů - numerická směrová diference
	- strategie restartu 
		- Metoda Hestenes-Stiefel - každých $r$ kroků
		- Broydenova metoda - při nespádovém směru
	- line search - metoda zlatého řezu
]
#slide(title: [Příklad B: Optimalizace funkce $f(x,y)$])[
	// - experiment 1.1-8 + obrazek vzdycky
	// - experiment 1 - default
	
	#experiment("1.1", [ 
		Metoda Hestenes-Stiefel
		#set par(first-line-indent: 0pt)
		#text(size: 18pt)[$f(x,y) = (x-y^2)^2 + x^2$] 
		#v(4pt)
		$limits(upright(a r g m i n)) f(x,y) = [0,0]$
	
		$epsilon = 10^(-5) space.quad delta = 10^(-13) space.quad epsilon_(1 D) = 10^(-16) space.quad a = 20 space.quad space i_(m a x) = 300 space.quad S_0 = I space.quad space.third r=4$
		#v(4pt)
	
		#table(
			columns: (auto, auto, auto, 21%),
			rows: (auto, 2.2em, 2.2em, 2.2em, 2.2em),
			[počáteční aproximace $x_0$], [nalezené minimum], [počet iterací], [konvergence],
			[$[5,5]$], [$vec(0.000029867565719, 0.007728264142202)$], [20], [ano],
			[$[99,5]$], [$vec(0.000107948936051, 0.014779163675676)$], [24], [ano],
			[$[5,-99]$], [$vec(0.000100949259804,-0.014076620781053)$], [19], [ano],
			[$[-50,50]$], [$vec(0.000117259377116, -0.015308227746036)$], [9], [ano],
		)
	])
]
