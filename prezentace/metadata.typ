// forked from https://github.com/FlixCoder/typst-slides

#let beamer_format = (16, 9)
#let theme_background = blue.darken(30%)
#let theme_text = white
#let font = "Open Sans"

#let presentation_title = "Semestrální práce z předmětu KMA/MNO"
#let presentation_subtitle = ""

#let author = "Jakub Koňařík"
#let date = "12.01.2023"
