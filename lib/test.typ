#import "./template.typ": *

// #show: thmrules  // dont forget this
// #set text(font: "New Computer Modern", size: 12pt)

#show: DefaultLook

= Tests for the library module
(scroll to the next page)

#pagebreak()

//check title page
#TitulniStrana("Kokos nazev hodne hlubokej a odbornej jakoze fakt dost opravdu", "Kokos Konarik", "Zapadoceska univerzita v Plzni", "Fakulta aplikovanych ved", "Katedra matematiky", "2023/24", "Bakalářská práce")

//check theorems.typ
#theorem("Lorem")[Theorems are working!]
#priklad[Scroll to the next page...]
#pagebreak()

//check abstrakt
#Abstrakt(lorem(250), lorem(10))

//check abstract
#Abstract(lorem(30), lorem(10))

//check prohlaseni
#Prohlaseni(lorem(40), "Jakub Kokos")

//check podekovani
#Podekovani(lorem(20), lorem(20)) 

//check obsah
#set text(fill: white)
== Halo halo
== Haloo
=== Anoo?
= Ooooooo
== Ooo anooo
== Brainmelt
=== Rotting
#set text(fill: black)
#v(-180pt)
#Obsah() 

//check zadani
#Zadani()

