#import "../theorems/theorems.typ": *
#import "@preview/oxifmt:0.2.0": strfmt

#let tweaks(font: "New Computer Modern", size: 11pt, margin: 1.25in, matrix-delim: "[", zadani: true, reference: true, obsah: true, heading-numbering: "1.1  ", page-numbering: "1", font-mono: "New Computer Modern Mono", first-line-indent: 1.8em, justify: true, equation-numbering: "(1)", line-spacing: 0.7em, paragraph-spacing: 1.1em, figure-caption-font-size: 9.5pt, prohlaseni: true, abstrakt: true, abstract: true, podekovani: true, pocet-stran-zadani: 1, format-zadani: "svg", zadani-scale: 120%, vsechny-reference: true, styl-reference: "../data/custom.csl", bibliography-filetype: "yml", references-title: "References", tisk: false, tisk-margin: 0.6cm) = (
		font: font,
		size: size,
		margin: margin,
		matrix-delim: matrix-delim,
		zadani: zadani,
		reference: reference,
		obsah: obsah,
		heading-numbering: heading-numbering,
		page-numbering: page-numbering,
		font-mono: font-mono,
		first-line-indent: first-line-indent,
		justify: justify,
		equation-numbering: equation-numbering,
		line-spacing: line-spacing,
		paragraph-spacing: paragraph-spacing,
		figure-caption-font-size: figure-caption-font-size,
		prohlaseni: prohlaseni,
		abstrakt: abstrakt,
		abstract: abstract,
		podekovani: podekovani,
		pocet-stran-zadani: pocet-stran-zadani,
		format-zadani: format-zadani,
		zadani-scale: zadani-scale,
		vsechny-reference: vsechny-reference,
		styl-reference: styl-reference,
		bibliography-filetype: bibliography-filetype,
		references-title: references-title,
		tisk: tisk,
		tisk-margin: tisk-margin,
	)
}

#let LanguageSetup(config) = { //TODO
	// set text(lang: "cs")
	// set text(lang: "en")
	let language = config.jazyk
	let multilingual = false
	if type(language) == string {set text(lang: language)} else if type(language) == array { if language.len() == 2 {set text(lang: language.at(0))}; multilingual = true }
	return multilingual
}

#let DefaultLook(body) = {
	let autor = "Kuba"  //temp-fix
	let nazev = "Ahoj"  //temp-fix
	set document(author: autor, title: nazev)
	set text(font: "New Computer Modern", size: 11pt)
	set page(numbering: "1", margin: 1.3in)
	counter(page).update(1)
	set heading(numbering: "1.1  ", outlined: true)
	set par(leading: 0.87em, first-line-indent: 1.8em, justify: true, linebreaks: "optimized")
	
	set math.equation(numbering: "(1)", supplement: none)
	set math.mat(column-gap: 1.5em, delim: "[", row-gap: 0.7em)

	show par: set block(spacing: 1.1em)
	show raw: set text(font: "Source Code Pro")
	show heading: set block(above: 1.6em, below: 1.2em)
	
	show: thmrules
	// show figure: it => pad(y: 15pt, it)
	show figure.caption: set text(size: 9.5pt)

	body
}

#let TitulniStrana(nazev, autor, univerzita, fakulta, katedra, rok, typ-prace, pbreak: true, tisk-margin: 0pt, tisk: false) = {
	let TitulniStrana1 = {
		let margin = if tisk == false {1in} else if tisk == "jednostranne" {(left: 1in + tisk-margin, right: 1in, y: 1in)} else if tisk == "oboustranne" {(y: 1in, outside: 1in, inside: 1in + tisk-margin)} else {panic("tisk spatne")}
		set page(margin: margin, numbering: none)
		set par(leading: 0.55em, first-line-indent: 1.8em, justify: true, linebreaks: "optimized") //idk
		set heading(outlined: false, numbering: none) //idk
		
		align(center + horizon)[
				#block(text(weight: 100, 2.4em, [*#upper(typ-prace)*]))
				#v(2em)
	    	#block(text(weight: 50, 2.0em, nazev))
	  	]
	  	place(top + center, pad(top: 0em, text(1.7em, [#univerzita])))
	  	place(top + center, pad(top: 3em, text(1.7em, [#fakulta])))
	  	place(top + center, pad(top: 6em, text(1.7em, [#katedra])))
		place(top + center, pad(top: 10em, image("../data/fakulta-logo.png", width: 20%)))
		place(bottom + right, [#autor])
		place(bottom + left, [#rok])
	}
	TitulniStrana1
	if pbreak == true {pagebreak()}
}

#let Abstrakt(text, slova, pbreak: true, tisk-margin: 0pt, tisk: false) = {
	let Abstrakt1 = {
		let margin = if tisk == false {(x: 1.3in, y: 2in)} else if tisk == "jednostranne" {(left: 1.3in + tisk-margin, right: 1.3in, y: 2in)} else if tisk == "oboustranne" {(y: 2in, outside: 1.3in, inside: 1.3in + tisk-margin)} else {panic("tisk spatne")}
		set page(margin: margin, numbering: none)
		set par(leading: 0.75em, first-line-indent: 1.8em, justify: true, linebreaks: "optimized")
		set heading(outlined: false, numbering: none)

		grid(
			columns: (auto),
			rows: (75%, 25%),
			[= Abstrakt
			#text],
			[= Klíčová slova	 
			#slova],
		)
	}
	Abstrakt1
	if pbreak == true {pagebreak()}
}

#let Abstract(text, keywords, pbreak: true, tisk-margin: 0pt, tisk: false) = {
	let Abstract1 = {
		let margin = if tisk == false {(x: 1.3in, y: 2in)} else if tisk == "jednostranne" {(left: 1.3in + tisk-margin, right: 1.3in, y: 2in)} else if tisk == "oboustranne" {(y: 2in, outside: 1.3in, inside: 1.3in + tisk-margin)} else {panic("tisk spatne")}
		set page(margin: margin, numbering: none)
		set par(leading: 0.75em, first-line-indent: 1.8em, justify: true, linebreaks: "optimized")
		set heading(outlined: false, numbering: none)

		grid(
			columns: (auto),
			rows: (75%, 25%),
			[= Abstract
			#text],
			[= Key words	 
			#keywords],
		)
	}
	Abstract1
	if pbreak == true {pagebreak()}
}

#let Prohlaseni(text, jmeno, pbreak: true, tisk-margin: 0pt, tisk: false) = {
	// let today = datetime.today()
	let Prohlaseni1 = {
		let margin = if tisk == false {(x: 1.3in, y: 2in)} else if tisk == "jednostranne" {(left: 1.3in + tisk-margin, right: 1.3in, y: 2in)} else if tisk == "oboustranne" {(y: 2in, outside: 1.3in, inside: 1.3in + tisk-margin)} else {panic("tisk spatne")}
		set page(margin: margin, numbering: none)
		set par(leading: 0.75em, first-line-indent: 1.8em, justify: true, linebreaks: "optimized")
		set heading(outlined: false, numbering: none)
		
		[= Prohlášení]
		v(1em)
		text
		v(2em)
		linebreak()
		[V Plzni, dne  #box(width: 100pt, repeat[.])]
		align(right, box(width: 140pt, repeat[.]))
		align(right, box(width: 140pt, align(center,[#jmeno])))
	}
	Prohlaseni1
	if pbreak == true {pagebreak()}
}

#let Podekovani(textCZ, textEN, pbreak: false, tisk-margin: 0pt, tisk: false) = {
	let Podekovani1 = {
		let margin = if tisk == false {(x: 1.3in, y: 2in)} else if tisk == "jednostranne" {(left: 1.3in + tisk-margin, right: 1.3in, y: 2in)} else if tisk == "oboustranne" {(y: 2in, outside: 1.3in, inside: 1.3in + tisk-margin)} else {panic("tisk spatne")}
		set page(margin: margin, numbering: none)
		set par(leading: 0.75em, first-line-indent: 1.8em, justify: true, linebreaks: "optimized")
		set heading(outlined: false, numbering: none)

		grid(
			columns: auto,
			rows: (50%, 50%),
			[= Acknowledgement
			#textEN],
			[= Poděkování 
			#textCZ],
		)
	}
	Podekovani1
	// if pbreak == true {pagebreak()}
}

#let Obsah(pbreak: true) = {
	let Obsah1 = {
		set page(numbering: none)
		let scale = 1 // kladne cislo

		show outline.entry.where(level: 1): it => {
			[#set text(size: scale*0.1em, fill: white); :) :) :) #linebreak()]
	  		strong(it)
			v(-1.25em + scale*0.3em)
		}
		show outline.entry.where(level: 2): it => {
			it 
			v(-1.3em + scale*0.3em)
		}
		show outline.entry.where(level: 3): it => {
			it 
			v(-1.3em + scale*0.3em)
		}
		show outline.entry.where(level: 4): it => {
			it 
			v(-1.3em + scale*0.3em)
		}
		show outline.entry.where(level: 5): it => {
			it 
			v(-1.3em + scale*0.3em)
		}

		outline(
			indent: n => {
				if n <= 0 []
				else if n == 1 [#h(1.8em)]
				else [#h(4.1em)]
			},
			fill: repeat([#h(0.01em).#h(0.01em)]) 
		)

		set page(numbering: none, margin: 1.3in)
	}
	Obsah1
	if pbreak == true {pagebreak()}
}

#let Zadani(pocet-stran: 1, filename: "zadani", filetype: "svg", scale: 120%, pbreak: false, tisk-margin: 0pt, tisk: false) = {
	set page(numbering: none, margin: 1.3in)
	if pocet-stran <= 0 { return }
	else if pocet-stran == 1 {
		align(
			center+horizon,
			image(strfmt("../../{}.{}",filename, filetype), width: scale)
		)
	} else {
		for i in range(1, pocet-stran + 1) {
			align(
				center+horizon,
				image(strfmt("../../{}-{}.{}",filename, i, filetype), width: scale)
			)
		}
	}
	if pbreak == true {pagebreak()}
}

#let indent() = [#h(2em)]




// //##############################################################
// //#                                                            #
// //#                       SEMESTRÁLNÍ                          #
// //#                          PRÁCE                             #
// //#                                                            #
// //##############################################################

// #let semestralka(nazev, autor, id, univerzita, fakulta, katedra, rok, body) = {
// 	set document(author: autor, title: "semestrální práce")
// 	set page(margin: 1in)
// 	set par(leading: 0.55em, first-line-indent: 1.8em, justify: true, linebreaks: "optimized")
// 	set text(font: "New Computer Modern", size: 11pt)
// 	set text(lang: "cs")
// 	// set math.equation(numbering: "(1)", supplement: none)
// 	set heading(outlined: false)
// 	set math.mat(column-gap: 1.5em, delim: "[", row-gap: 0.7em)
// 	set outline(
// 		indent: n => {
// 			if n == 0 []
// 			else if n == 1 [#h(21pt)]
// 			else [#h(48pt)]
// 		},
// 		fill: repeat([#math.space.hair.#math.space.third]) 
// 	)

// 	show par: set block(spacing: 1.1em)
// 	show raw: set text(font: "Fira Code", size: 9.9pt, weight: 460)
// 	show heading: set block(above: 1.7em, below: 1em)
// 	show: thmrules
	
// 	// show outline.entry.where(level: 1): it => {
// 	// 	// set text(size: 5pt)
// 	// 	// linebreak()
// 	// 	// set text(size: 12pt)
// 	// 	strong(it)
// 	// }
	
// 	show outline.entry.where(level: 1): it => {
// 		set text(size: 8pt)
// 		set text(size: 12pt)
//   		strong(it)
// 	}
// 	show outline.entry.where(level: 2): it => {
// 		it 
// 	}
// 	show outline.entry.where(level: 3): it => {
// 		it 
// 		linebreak()
// 		set text(size: 0.1pt)
//   		hide(it)
// 		set text(size: 12pt)
// 	}
// 	show figure: it => {
// 		pad(y: 10pt, it)
// 	}
	
// 	TitulniStrana(nazev, autor, id, univerzita, fakulta, katedra, rok)
// 	Zadani()
// 	set page(margin: 0.7in)
// 	// Obsah()
// 	// set page(numbering: "1")
// 	// counter(page).update(1)
// 	set heading(outlined: true)
// 	set heading(numbering: "1.1  ")
// 	set par(leading: 0.87em)

// 	body
// }



// //##############################################################
// //#                                                            #
// //#                       PREZENTACE                           #
// //#                                                            #
// //#   forked from https://github.com/FlixCoder/typst-slides    #
// //#                                                            #
// //##############################################################

// #import "presentation-metadata.typ": *


// #let beamer_format = (16, 9)
// #let theme_background = blue.darken(10%)
// #let theme_text = white
// #let font = "Open Sans"

// #let slides(nazev, podnazev, author, date, doc) = {
// 	let presentation_title = nazev
// 	let presentation_subtitle = poznazev

// 	let scale = 2cm
// 	let width = beamer_format.at(0) * scale
// 	let height = beamer_format.at(1) * scale
	
// 	// Setup.
// 	set document(
// 		title: presentation_title,
// 		author: author,
// 	)
// 	set text(
// 		font: font,
// 		size: 25pt,
// 	)
// 	set page(
// 		width: width,
// 		height: height,
// 		margin: 0pt,
// 	)
// 	set align(center + horizon)
	
// 	show heading: title => {
// 		pagebreak(weak: true)
// 		rect(
// 			width: 100%, height: 100%,
// 			fill: theme_background,
// 			text(fill: theme_text, title)
// 		)
// 		counter(page).update(x => x - 1)
// 		pagebreak(weak: true)
// 	}
	
// 	// Title page.
// 	rect(
// 		width: 100%, height: 100%,
// 		fill: theme_background,
// 		{
// 			set text(fill: theme_text)
// 			text(size: 50pt, weight: "bold", presentation_title)
// 			linebreak()
// 			text(size: 30pt, presentation_subtitle)
// 			v(2em)
// 			text(size: 25pt, author)
// 			linebreak()
// 			text(size: 15pt, date)
// 		}
// 	)
// 	// pagebreak(weak: true)
// 	counter(page).update(1)
	
// 	// Actual content.
// 	doc
// }

// #let slide(title: "", content) = locate(loc => {
// 	// Header with slide title.
// 	let header = {
// 		let headers = query(selector(heading).before(loc), loc)
// 		set align(left + top)
// 		set text(fill: theme_text, weight: "bold")
// 		rect(width: 100%, fill: theme_background, pad(x: 10pt, y: 10pt)[
// 			#if headers == () {
// 				text(size: 30pt, title)
// 			} else {
// 				let section = headers.last().body
// 				if title == "" {
// 					text(size: 30pt, section)
// 				} else {
// 					text(size: 15pt, section)
// 					linebreak()
// 					text(size: 25pt, title)
// 				}
// 			}
// 		])
// 	}
	
// 	// Footer with left and right section.
// 	let footer = grid(columns: (1fr, auto), pad(x: 5pt, y: 8pt)[
// 		// Presentation title and author.
// 		#set align(left)
// 		#set text(12pt)
// 		#presentation_title \
// 		#set text(10pt)
// 		#author -- #date
// 	], [
// 		// Page counter.
// 		#rect(
// 			width: 60pt,
// 			height: 40pt,
// 			fill: theme_background,
// 			align(
// 				center + horizon,
// 				text(20pt, fill: theme_text, counter(page).display())
// 			)
// 		)
// 	])
	
// 	pagebreak(weak: true)
// 	grid(rows: (auto, 1fr, auto), {
// 		header
// 	}, {
// 		// Inner slide content.
// 		pad(x: 10pt, y: 10pt, box(width: 95%, align(left, content)))
// 	}, {
// 		footer
// 	})
// 	// pagebreak(weak: true)
// })
