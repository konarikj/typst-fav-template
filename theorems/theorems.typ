// Forked from https://github.com/sahasatvik/typst-theorems

#let thmcounters = state("thm",
  (
    "counters": ("heading": ()),
    "latest": ()
  )
)


#let thmenv(identifier, base, base_level, fmt) = {

  let global_numbering = numbering

  return (
    ..args,
    body,
    number: auto,
    numbering: "1.1",
    refnumbering: auto,
    supplement: identifier,
    base: base,
    base_level: base_level
  ) => {
    let name = none
    if args != none and args.pos().len() > 0 {
      name = args.pos().first()
    }
    if refnumbering == auto {
      refnumbering = numbering
    }
    let result = none
    if number == auto and numbering == none {
      number = none
    }
    if number == auto and numbering != none {
      result = locate(loc => {
        return thmcounters.update(thmpair => {
          let counters = thmpair.at("counters")
          // Manually update heading counter
          counters.at("heading") = counter(heading).at(loc)
          if not identifier in counters.keys() {
            counters.insert(identifier, (0, ))
          }

          let tc = counters.at(identifier)
          if base != none {
            let bc = counters.at(base)

            // Pad or chop the base count
            if base_level != none {
              if bc.len() < base_level {
                bc = bc + (0,) * (base_level - bc.len())
              } else if bc.len() > base_level{
                bc = bc.slice(0, base_level)
              }
            }

            // Reset counter if the base counter has updated
            if tc.slice(0, -1) == bc {
              counters.at(identifier) = (..bc, tc.last() + 1)
            } else {
              counters.at(identifier) = (..bc, 1)
            }
          } else {
            // If we have no base counter, just count one level
            counters.at(identifier) = (tc.last() + 1,)
            let latest = counters.at(identifier)
          }

          let latest = counters.at(identifier)
          return (
            "counters": counters,
            "latest": latest
          )
        })
      })

      number = thmcounters.display(x => {
        return global_numbering(numbering, ..x.at("latest"))
      })
    }

    return figure(
      result +  // hacky!
      fmt(name, number, body, ..args.named()) +
      [#metadata(identifier) <meta:thmenvcounter>],
      kind: "thmenv",
      outlined: false,
      caption: name,
      supplement: supplement,
      numbering: refnumbering,
    )
  }
}


#let thmbox(
  identifier,
  head,
  ..blockargs,
  supplement: auto,
  padding: (x: 0pt, y: 0pt),
  namefmt: x => [(#x)],
  titlefmt: strong,
  bodyfmt: x => x,
  separator: [#h(0em):#h(0.2em)],
  base: "heading",
  base_level: none,
) = {
  if supplement == auto {
    supplement = head
  }
  let boxfmt(name, number, body, title: auto) = {
    if not name == none {
      name = [#namefmt(name)]
    } else {
      name = []
    }
    if title == auto {
      title = head
    }
    if not number == none {
      title += " " + number
    }
    title = titlefmt(title)
    body = bodyfmt(body)
    pad(
      ..padding,
      block(
        width: 100%,
        inset: 0em,
        radius: 0.3em,
        breakable: false,
        ..blockargs.named(),
        [#set align(left); #title#name#separator#body]
      )
    )
  }
  return thmenv(
    identifier,
    base,
    base_level,
    boxfmt
  ).with(
    supplement: supplement,
  )
}


#let thmplain = thmbox.with(
  padding: (top: -0.4em, bottom: -0.4em),
  breakable: true,
  inset: (top: 0em, left: 0em, right: 0em),
  namefmt: name => [ (#name)],
  bodyfmt: emph,
)



#let thmrules(doc) = {
  show figure.where(kind: "thmenv"): it => it.body

  show ref: it => {
    if it.element == none {
      return it
    }
    if it.element.func() != figure {
      return it
    }
    if it.element.kind != "thmenv" {
      return it
    }

    let supplement = it.element.supplement
    if it.citation.supplement != none {
      supplement = it.citation.supplement
    }

    let loc = it.element.location()
    let thms = query(selector(<meta:thmenvcounter>).after(loc), loc)
    let number = thmcounters.at(thms.first().location()).at("latest")
    return link(
      it.target,
      [#supplement~#numbering(it.element.numbering, ..number)]
    )
  }

  doc
}

#let theorem = thmplain("theorem", "Theorem")
#let veta = thmplain("theorem", "Věta")

#let conjecture = thmplain("theorem", "Conjecture")
#let hypoteza = thmplain("theorem", "Hypotéza")

#let lemma = thmplain("theorem", "Lemma")
#let observation = thmplain("theorem", "Observation")

#let claim = thmplain("theorem", "Claim")
#let tvrzeni = thmplain("theorem", "Tvrzení")

#let definition = thmplain("theorem", "Definition")
#let definice = thmplain("theorem", "Definice")

#let corollary = thmplain("corollary","Corollary",base: "theorem")
#let dusledek = thmplain("corollary","Důsledek",base: "theorem")

#let example = thmplain("theorem", "Example")
#let priklad = thmplain("theorem", "Příklad") 

#let problem = thmplain("theorem", "Problem") // co s cestinou ted?

#let proof = thmplain("proof","Proof",base: "theorem",titlefmt: it => emph(it) ,bodyfmt: body => [#body #h(0.8em) $square$]).with(numbering: none)
#let dukaz = thmplain("proof","Důkaz",base: "theorem",titlefmt: it => emph(it) ,bodyfmt: body => [#body #h(0.8em) $square$]).with(numbering: none)

#let define-thm = thmplain
