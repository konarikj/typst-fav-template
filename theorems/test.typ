#import "./theorems.typ": *
#show: thmrules

#set align(left)

= Testing theorems

#theorem("Veta o prdu")[prrrrrrd prd prrd prrrrrrdprrrrrrd prd prrd prrrrrrdprrrrrrd prd prrd prrrrrrdprrrrrrd prd prrd prrrrrrdprrrrrrd prd prrd prrrrrrd]
#proof[duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd ppppppp pp]
#lemma[prrrrrrd prd prrd prrrrrrd]
#corollary("Dusledek prdu")[prrrrrrd prd prrd prrrrrrd]
#conjecture("Hypoteza o prdu")[prrrrrrd prd prrd prrrrrrd]
#example[prrrrrrd prd prrd prrrrrrd]
#definition("Definice prdu")[prrrrrrd prd prrd prrrrrrd]


= Testy vět

#veta("Veta o prdu")[prrrrrrd prd prrd prrrrrrdprrrrrrd prd prrd prrrrrrdprrrrrrd prd prrd prrrrrrdprrrrrrd prd prrd prrrrrrdprrrrrrd prd prrd prrrrrrd]
#dukaz[duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd duuukaz prrrrrd ppppppp pp]
#lemma[prrrrrrd prd prrd prrrrrrd]
#dusledek("Dusledek prdu")[prrrrrrd prd prrd prrrrrrd]
#hypoteza("Hypoteza o prdu")[prrrrrrd prd prrd prrrrrrd]
#priklad[prrrrrrd prd prrd prrrrrrd]
#definice("Definice prdu")[prrrrrrd prd prrd prrrrrrd]
