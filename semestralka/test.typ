#import "template.typ": *

#let config = (
	predmet: "KMA/TSI",
	nazev: "Algebraicke vlastnosti grafů",
	autor: "Jakub Koňařík",
	studijni-cislo: "A21B0060P",
	univerzita: "Západočeská univerzita v Plzni",
	fakulta: "Fakulta aplikovaných věd",
	katedra: "Katedra matematiky",
	rok: "2023/24",
	jazyk: "cs",
)

#let tweaks = tweaks(reference: false)

#show: template.with(config, tweaks)



= Uživatelské prostředí
Práce obsahuje 3 spustitelné MatLab soubory. Soubor _prikladA.mlx_ obsahující řešení příkladu A, soubor _prikladB.mlx_ obsahující řešení příkladu B a soubor _example.mlx_ obsahující příklad spuštění metod numerické optimalizace užité v příkladech A a B.

#veta[Theorems fungujou]


Každý z těchto souborů lze spustit kliknutím na tlačítko _run_ v horní liště prostředí MatLab. V souboru _example.mlx_ najdeme 2 metody numerické optimalizace. Brozdenovu metodu a metodu Hestenes-Stiefel. Kliknutím na tlačítko _run_ uživatel spustí obě metody se stejnou funkcí, počáteční podmínkou a parametry. V případě, že by uživatel chtěl optimalizovat jinou funkci, stačí přepsat proměnnou _f_. Všechny metody očekávejí na vstupu funkci jedné vektorové proměnné. V sekci _Parametry_ může uživatel libovolně měnit parametry funkce. Parametry funkcí jsou popsány ve formě in-line komentářů za každou deklarovanou proměnnou.

#pagebreak()

= Optimalizace uživatelem zadané funkce
Řešení příkladu A obsahuje program, který pro uživatelem zadanou funkci $f$ najde minimum funkce $f$ pomocí Hestenes-Stiefelovy a Broydenovy metody. Ve výše uvedených metodách je uvedena line-search metoda Zlatého řezu. Při výpočtu také probíhá kontrola spádovosti směrů a průběh výpočtu se při optimalizaci 2D funkce automaticky vykresluje.  

Pro zadání funkce k optimalizaci slouží proměnná $f$. Zvolená počáteční aproximace je reprezentována proměnnou _pocatecni_approx_. Společně s funkcí $f$ je nutné uvést i její globální minimum do proměnné _min_ pro účely ilustrace průběhu výpočtu.

Parametry metod optimalizace jsem volil na základě numerických experimentů v příkladu B. Je pravděpodobné, že tyto parametry nebudou optimální pro libovolnou uživatelem zadanou funkci. V případě, že bude metoda divergovat z důvodu nesprávně zvolených parametrů pro danou úlohu je na uživateli parametry změnit.

== Metoda Hestenes-Stiefel
K optimalizaci uživatelem zadané funkce pomocí metody Hestenes-Stiefel použil toleranci zastavovací podmínky $epsilon = 10^(-5)$. Zastavovací podmínka je v obou metodách stejná a to norma zobecněného gradientu. Pro výpočet zobecněného gradientu jsem využil dopřednou diferenci s krokem $delta = 10^(-15)$. V metodě je využita line-search metoda Zlatého řezu s tolerancí zastavovací podmínky $epsilon_(1 D) = 10^(-20)$ a velikostí intervalu $a = 50$. Maximální počet iterací byl zvolen na $i_upright(m a x) = 300$. Restart metody se děje po čtyřech iteracích. 

Jako ukázkovou funkci jsem volil funkci $f(x,y) = (2-x)^2 + 10(y-x^2)^2$ s počáteční aproximací $x_0 = [5,5]$. Funkce $f$ má globální minimum v bodě $[2,4]$.  Průběh výpočtu je ilustrován v obrázku 1 vlevo. Metoda dala poměrně přesný výsledek po 11 iteracích.  

== Metoda Kokos-Kokosak
K optimalizaci uživatelem zadané funkce pomocí metody Hestenes-Stiefel použil toleranci zastavovací podmínky $epsilon = 10^(-5)$. Zastavovací podmínka je v obou metodách stejná a to norma zobecněného gradientu. Pro výpočet zobecněného gradientu jsem využil dopřednou diferenci s krokem $delta = 10^(-15)$. V metodě je využita line-search metoda Zlatého řezu s tolerancí zastavovací podmínky $epsilon_(1 D) = 10^(-20)$ a velikostí intervalu $a = 50$. Maximální počet iterací byl zvolen na $i_upright(m a x) = 300$. Restart metody se děje po čtyřech iteracích. 

Jako ukázkovou funkci jsem volil funkci $f(x,y) = (2-x)^2 + 10(y-x^2)^2$ s počáteční aproximací $x_0 = [5,5]$. Funkce $f$ má globální minimum v bodě $[2,4]$.  Průběh výpočtu je ilustrován v obrázku 1 vlevo. Metoda dala poměrně přesný výsledek po 11 iteracích.  

