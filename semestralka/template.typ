#import "../theorems/theorems.typ": *
#import "../lib/template.typ": *

#let TitulniStrana(predmet, nazev, autor, id, univerzita, fakulta, katedra, rok) = {
	let titulniStrana1 = {
		set page(margin: 1in)
		align(center + horizon)[
			#block(text(weight: 100, 2.7em, [SEMESTRÁLNÍ PRÁCE Z PŘEDMĚTU #predmet]))
	  	]
	  	place(top + center, pad(top: 0pt, text(1.7em, [#univerzita])))
	  	place(top + center, pad(top: 35pt, text(1.7em, [#fakulta])))
	  	place(top + center, pad(top: 70pt, text(1.7em, [#katedra])))
		place(top + center, pad(top: 130pt, image("../data/fakulta-logo.png", width: 20%)))
		place(bottom + left, [#autor, #id])
		place(bottom + right, [#rok])
	}
	let titulniStrana2 = {
		set page(margin: 1in)
		align(center + horizon)[
			#block(text(weight: 100, 3.6em, [#predmet]))
			#v(1.8em)
			#block(text(weight: 100, 2.3em, [#nazev]))
	  	]
	  	place(top + center, pad(top: 0pt, text(1.7em, [#univerzita])))
	  	place(top + center, pad(top: 35pt, text(1.7em, [#fakulta])))
	  	place(top + center, pad(top: 70pt, text(1.7em, [#katedra])))
		place(top + center, pad(top: 130pt, image("../data/fakulta-logo.png", width: 20%)))
		place(bottom + left, [#autor, #id])
		place(bottom + right, [#rok])
	}	
	titulniStrana2
	pagebreak()
}

#let indent = [$space.quad space$]

#let template(config, tweaks, body) = {
	set document(author: config.autor, title: config.nazev)
	set par(leading: tweaks.line-spacing, first-line-indent: tweaks.first-line-indent, justify: tweaks.justify, linebreaks: "optimized")
	set text(font: tweaks.font, size: tweaks.size, lang: config.jazyk)
	set math.equation(numbering: tweaks.equation-numbering, supplement: none)
	set math.mat(column-gap: 1.5em, delim: tweaks.matrix-delim, row-gap: 0.7em)

	show par: set block(spacing: tweaks.paragraph-spacing)
	show raw: set text(font: tweaks.font-mono)
	show heading: set block(above: 1.6em, below: 1.2em)
	show: thmrules
	show figure.caption: set text(size: tweaks.figure-caption-font-size)
	show figure: it => pad(y: 0.7em, it)


	TitulniStrana(config.predmet, config.nazev, config.autor, config.studijni-cislo, config.univerzita, config.fakulta, config.katedra, config.rok)
	
	if tweaks.zadani == true { Zadani(pocet-stran: tweaks.pocet-stran-zadani, scale: tweaks.zadani-scale, filetype: tweaks.format-zadani) }
	set page(margin: tweaks.margin)
	if tweaks.obsah == true { Obsah() }
	
	set page(numbering: tweaks.page-numbering)
	counter(page).update(1)
	set heading(numbering: tweaks.heading-numbering, outlined: true)

	body

	if tweaks.reference == true {
		pagebreak()
		bibliography("../../bibliography.yml", title: "References", style: "ieee")
	}
}

// #let template(nazev, autor, id, univerzita, fakulta, katedra, rok, body) = {
// 	set document(author: autor, title: "semestrální práce")
// 	set page(margin: 1in)
// 	set par(leading: 0.55em, first-line-indent: 1.8em, justify: true, linebreaks: "optimized")
// 	set text(font: "New Computer Modern", size: 11pt)
// 	set text(lang: "cs")
// 	// set math.equation(numbering: "(1)", supplement: none)
// 	set heading(outlined: false)
// 	set math.mat(column-gap: 1.5em, delim: "[", row-gap: 0.7em)
// 	// set outline(
// 	// 	indent: n => {
// 	// 		if n == 0 []
// 	// 		else if n == 1 [#h(21pt)]
// 	// 		else [#h(48pt)]
// 	// 	},
// 	// 	fill: repeat([#math.space.hair.#math.space.third]) 
// 	// )

// 	show par: set block(spacing: 1.1em)
// 	show raw: set text(font: "Fira Code", size: 9.9pt, weight: 460)
// 	show heading: set block(above: 1.7em, below: 1em)
// 	show: thmrules
	
// 	// show outline.entry.where(level: 1): it => {
// 	// 	// set text(size: 5pt)
// 	// 	// linebreak()
// 	// 	// set text(size: 12pt)
// 	// 	strong(it)
// 	// }
	
// 	// show outline.entry.where(level: 1): it => {
// 	// 	set text(size: 8pt)
// 	// 	set text(size: 12pt)
//  //  		strong(it)
// 	// }
// 	// show outline.entry.where(level: 2): it => {
// 	// 	it 
// 	// }
// 	// show outline.entry.where(level: 3): it => {
// 	// 	it 
// 	// 	linebreak()
// 	// 	set text(size: 0.1pt)
//  //  		hide(it)
// 	// 	set text(size: 12pt)
// 	// }
// 	// show figure: it => {
// 	// 	pad(y: 10pt, it)
// 	// }
	
// 	TitulniStrana(nazev, autor, id, univerzita, fakulta, katedra, rok)
// 	Zadani()
// 	set page(margin: 0.7in)
// 	Obsah()
// 	// set page(numbering: "1")
// 	// counter(page).update(1)
// 	set heading(outlined: true)
// 	set heading(numbering: "1.1  ")
// 	set par(leading: 0.87em)

// 	body
// }

