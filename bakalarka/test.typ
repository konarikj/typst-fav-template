#import "template.typ": *
#import "../../nalezitosti.typ": *

#let config = (
	nazev: "Union-closed sets conjecture and the strength of constraints in linear programming",
	autor: "Jakub Koňařík",
	univerzita: "University of West Bohemia",
	fakulta: "Faculty of Applied Sciences",
	katedra: "Department of Mathematics",
	rok: "2024",
	jazyk: "en",
	abstrakt: abstrakt,
	abstract: abstract,
	klicova-slova: klicova-slova,
	key-words: key-words,
	prohlaseni: prohlaseni,
	acknowledgement: acknowledgement,
	podekovani: podekovani,
)

#let tweaks = tweaks(pocet-stran-zadani: 2)

#show: template.with(config, tweaks)

#set heading(numbering: none)
= Notation
- $RR$, $RR^n$, $RR^(m times n)$
- $c^T$
- $union$, $emptyset$, $in$
= Elementary definitions
- graph
- hypergraph
- lattice
- family of sets
- probability stuff needed for gilmer
- minimum/maximum
- NP-Hard?
- set?
#set heading(numbering: "1.1  ")

= Introduction
== Union-closed sets conjecture

The Union-closed sets conjecture is a famous conjecture from extremal combinatorics that deals with families of sets closed under union.
A family of sets $cal(F)$ is union-closed if for every two member sets $A,B in cal(F)$ their union $A union B$ in also contained in $cal(F)$.
An example of a union-closed family of sets is shown in [...]. 


has an element that is contained in at least half of the member-sets.]<the-conjecture>

Despite its simplicity, the conjecture has proven to be quite challenging and remains an open problem in mathematics. 
Among other things, this thesis aims to prove a case of the conjecture when the size of the largest member set in $cal(F)$ is bounded.

#strike[Is easy to prove forcertain types of families of sets [examples], but a general case is still unsolved. An example union closed family is shown in (blabla). ]

The conjecture can be formulated in several equivalent ways using multiple structures from disctrete mathematics, as discussed in @formulations. The conjecture's significance thus lies in it's connection to different areas of mathematics.


== Equivalent formulations of the Union-closed sets conjecture<formulations>
#conjecture("")[Any finite intersection-closed family of at least two sets has an element that is contained in at most half of the member-sets]


#conjecture("")[Let $L$ be a finite lattice with at least two elements. Then there
is a join-irreducible element a with $abs([a)) ≤ 1/2 abs(L)$.]

#conjecture("")[Any bipartite graph with at least one edge contains in each of
its bipartition classes a vertex that lies in at most half of the maximal stable
sets]

#conjecture("")[Any normalised family $N != {emptyset}$ contains a
basis set $B$ of size $|B| ≥ 1/2 |N|$.]

linebreak()

= Test
== Test
=== Test
==== Test
