#import "@preview/oxifmt:0.2.0": strfmt   // TODO jestli uz to neni ve scopu
#import "../theorems/theorems.typ": *   // nevim jestli je nutny
#import "../lib/template.typ": *

#let indent = [$space.quad space$]

#let template(config, tweaks, body) = {
	set document(author: config.autor, title: config.nazev)
	set par(leading: tweaks.line-spacing, first-line-indent: tweaks.first-line-indent, justify: tweaks.justify, linebreaks: "optimized")
	set text(font: tweaks.font, size: tweaks.size, lang: config.jazyk)
	set math.equation(numbering: tweaks.equation-numbering, supplement: none)
	set math.mat(column-gap: 1.5em, delim: tweaks.matrix-delim, row-gap: 0.7em)

	// margin = if type(tweaks.margin) == length {(left: tweaks.margin, right: tweaks.margin, top: tweaks.margin, bottom: tweaks.margin)}
	set page(margin: tweaks.margin, numbering: tweaks.page-numbering)

	show par: set block(spacing: tweaks.paragraph-spacing)
	show raw: set text(font: tweaks.font-mono, size: 1.22em)
	show heading: set block(above: 2em, below: 1.2em)
	show heading: set text(size: 1.1em)
	show: thmrules
	show figure.caption: set text(size: tweaks.figure-caption-font-size)
	show figure: it => pad(y: 0.7em, it)

	let typ-prace = if config.jazyk == "en" {"Bachelor's thesis"} else if config.jazyk == "cs" {"bakalářská práce"} else {"Chyba v prekladu"}
	TitulniStrana(config.nazev, config.autor, config.univerzita, config.fakulta, config.katedra, config.rok, typ-prace, tisk: tweaks.tisk, tisk-margin: tweaks.tisk-margin)

	if tweaks.zadani == true { if tweaks.tisk == false { Zadani(pocet-stran: tweaks.pocet-stran-zadani, scale: tweaks.zadani-scale, filetype: tweaks.format-zadani, tisk: tweaks.tisk, tisk-margin: tweaks.tisk-margin) }}
	if tweaks.abstract == true { Abstract(config.abstract,config.key-words, tisk: tweaks.tisk, tisk-margin: tweaks.tisk-margin) }
	set text(lang: "cs")  // TODO
	if tweaks.abstrakt == true { Abstrakt(config.abstrakt,config.klicova-slova, tisk: tweaks.tisk, tisk-margin: tweaks.tisk-margin) }
	if tweaks.podekovani == true { Podekovani(config.podekovani, config.acknowledgement, tisk: tweaks.tisk, tisk-margin: tweaks.tisk-margin) }
	if tweaks.prohlaseni == true { Prohlaseni(config.prohlaseni, config.autor, tisk: tweaks.tisk, tisk-margin: tweaks.tisk-margin) }
	set text(lang: config.jazyk)
	if tweaks.obsah == true { Obsah() }
	
	counter(page).update(1)
	set heading(numbering: tweaks.heading-numbering, outlined: true)

	body

	if tweaks.reference == true {
		pagebreak()
		set par(first-line-indent: 0pt, leading: 0.5em) // TODO add to tweaks
		bibliography(strfmt("../../bibliography.{}", tweaks.bibliography-filetype), title: tweaks.references-title, style: tweaks.styl-reference, full: tweaks.vsechny-reference)
	}
}

// #let template(config, body) = {
// 	set document(author: config.autor, title: config.nazev)
// 	set page(margin: 1in)
// 	set par(leading: 0.55em, first-line-indent: 1.8em, justify: true, linebreaks: "optimized")
// 	set text(font: "New Computer Modern", size: 11pt)
// 	set math.equation(numbering: "(1)", supplement: none)
// 	set heading(outlined: false)
// 	set math.mat(column-gap: 1.5em, delim: "[", row-gap: 0.7em)

// 	show par: set block(spacing: 1.1em)
// 	show raw: set text(font: "New Computer Modern Mono")
// 	show heading: set block(above: 1.6em, below: 1.2em)
// 	show: thmrules
// 	show figure.caption: set text(size: 9.5pt)

// 	let typ-prace = if config.jazyk == "en" {"Bachelor's thesis"} else if config.jazyk == "cs" {"bakalářská práce"} else {"Chyba v prekladu"}
// 	TitulniStrana(config.nazev, config.autor, config.univerzita, config.fakulta, config.katedra, config.rok, typ-prace)
// 	set page(margin: (x: 1.5in, y: 2.3in))
// 	Zadani()
// 	set text(lang: "cs")
// 	Prohlaseni(config.prohlaseni, config.autor)
// 	set text(lang: config.jazyk)
// 	Abstrakt(config.abstrakt,config.klicova-slova)
// 	Abstract(config.abstract,config.key-words)
// 	Podekovani(config.podekovani,config.acknowledgement)
// 	set page(margin: 1.3in)
// 	Obsah()
	
// 	set page(numbering: "1")
// 	counter(page).update(1)
// 	set heading(outlined: true)
// 	set heading(numbering: "1.1  ")
// 	set par(leading: 0.87em)

// 	body

// 	pagebreak()
// 	bibliography("../../bibliography.yml", title: "References", style: "ieee")
// }





// //##############################################################
// //#                                                            #
// //#                       PREZENTACE                           #
// //#                                                            #
// //#   forked from https://github.com/FlixCoder/typst-slides    #
// //#                                                            #
// //##############################################################

// #import "presentation-metadata.typ": *


// #let beamer_format = (16, 9)
// #let theme_background = blue.darken(10%)
// #let theme_text = white
// #let font = "Open Sans"

// #let slides(nazev, podnazev, author, date, doc) = {
// 	let presentation_title = nazev
// 	let presentation_subtitle = podnazev

// 	let scale = 2cm
// 	let width = beamer_format.at(0) * scale
// 	let height = beamer_format.at(1) * scale
	
// 	// Setup.
// 	set document(
// 		title: presentation_title,
// 		author: author,
// 	)
// 	set text(
// 		font: font,
// 		size: 25pt,
// 	)
// 	set page(
// 		width: width,
// 		height: height,
// 		margin: 0pt,
// 	)
// 	set align(center + horizon)
	
// 	show heading: title => {
// 		pagebreak(weak: true)
// 		rect(
// 			width: 100%, height: 100%,
// 			fill: theme_background,
// 			text(fill: theme_text, title)
// 		)
// 		counter(page).update(x => x - 1)
// 		pagebreak(weak: true)
// 	}
	
// 	// Title page.
// 	rect(
// 		width: 100%, height: 100%,
// 		fill: theme_background,
// 		{
// 			set text(fill: theme_text)
// 			text(size: 50pt, weight: "bold", presentation_title)
// 			linebreak()
// 			text(size: 30pt, presentation_subtitle)
// 			v(2em)
// 			text(size: 25pt, author)
// 			linebreak()
// 			text(size: 15pt, date)
// 		}
// 	)
// 	// pagebreak(weak: true)
// 	counter(page).update(1)
	
// 	// Actual content.
// 	doc
// }

// #let slide(title: "", content) = locate(loc => {
// 	// Header with slide title.
// 	let header = {
// 		let headers = query(selector(heading).before(loc), loc)
// 		set align(left + top)
// 		set text(fill: theme_text, weight: "bold")
// 		rect(width: 100%, fill: theme_background, pad(x: 10pt, y: 10pt)[
// 			#if headers == () {
// 				text(size: 30pt, title)
// 			} else {
// 				let section = headers.last().body
// 				if title == "" {
// 					text(size: 30pt, section)
// 				} else {
// 					text(size: 15pt, section)
// 					linebreak()
// 					text(size: 25pt, title)
// 				}
// 			}
// 		])
// 	}
	
// 	// Footer with left and right section.
// 	let footer = grid(columns: (1fr, auto), pad(x: 5pt, y: 8pt)[
// 		// Presentation title and author.
// 		#set align(left)
// 		#set text(12pt)
// 		#presentation_title \
// 		#set text(10pt)
// 		#author -- #date
// 	], [
// 		// Page counter.
// 		#rect(
// 			width: 60pt,
// 			height: 40pt,
// 			fill: theme_background,
// 			align(
// 				center + horizon,
// 				text(20pt, fill: theme_text, counter(page).display())
// 			)
// 		)
// 	])
	
// 	pagebreak(weak: true)
// 	grid(rows: (auto, 1fr, auto), {
// 		header
// 	}, {
// 		// Inner slide content.
// 		pad(x: 10pt, y: 10pt, box(width: 95%, align(left, content)))
// 	}, {
// 		footer
// 	})
// 	// pagebreak(weak: true)
// })
